### Note

Migrated to: https://gitlab.com/aadityabagga/openrc-services

### Summary

Based upon [packages-openrc](https://github.com/manjaro/packages-openrc), this repository contains a set of OpenRC services with scripts to download and install the services.

Currently these can be used on Slackware Linux.

### Links

* Slackbuilds: https://slackbuilds.org/repository/14.2/system/openrc-services/

* Documentation: http://docs.slackware.com/howtos:general_admin:openrc
